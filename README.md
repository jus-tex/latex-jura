# LaTeX jura

Dies ist eine Dokumentklasse für LATEX 2ε zum Schreiben von juristischen Hausarbeiten.

## Benutzung

Um die jura-Dokumentenklasse benutzen zu können, müssen erst die beiden Dateien
jura.cls und alphanum.sty erzeugt werden. Dies geschieht indem man TEX mit der
Datei jura.ins aufruft. Dabei wird gleichzeitig ein Testdokument mit Namen juratest.ltx
erzeugt, das die Verwendung aller Funktionen der Klasse demonstriert. Die so erzeugten
Dateien legt man dann in ein Verzeichnis, in dem LATEX sie später findet.

## License

Copyright (C) 2017 by Philipp.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as published 
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

This material is copyrighted by Felix Braun. However, rights are granted
according to version 2 of the GNU Public Licence (GPL).

This means in essence:
- this file is freely distributable provided that it is not modified
- it may be sold
- it may be modified provided that the result is also placed under the GPL.

(wherever these terms divert from the GPL, the latter shall prevail)
